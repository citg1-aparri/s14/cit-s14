package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.ArrayList;

@SpringBootApplication
@RestController
public class DiscussionApplication {
	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}
}
